
class Book {

     constructor(title, genre, author, read = false, dateRead = null) {

          this.title = title;           //title of book
          this.genre = genre;           //genre of book
          this.author = author;         //author of book

          if (read) {                   //execute while book's state is read

               this.haveRead(dateRead);
          }
     }

     //method set book' state is read and reading time
     //if date is invalid, dateRead assign for time now
     haveRead(dateRead) {

          this.dateRead = true;
          if (!isNaN(Date.parse(dateRead))) {

               this.dateRead = new Date(dateRead);
               return this;
          }
          else {

               this.dateRead = new Date(Date.now());
          }
     }
}

class BookList {

     constructor() {

          this.listUnreadBook = [];     //list book isn't read
          this.curBook = null;          //the book is currently read
          this.completeBook = [];       //list book is complete
          this.nextBook = null;         //the book will be read next
     }

     //if curBook is null, curbook is assigned for argument(book)
     //else argument is pushed into listUnreadBook
     add(book) {

          if (book instanceof Book) {

               if (this.curBook === null) {

                    this.curBook = book;
               }
               else {

                    this.listUnreadBook.push(book);
               }
          }
     }

     //after curBook is complete, book is going to have read time curBook is assigned next book
     //if listUnreadBook is null, the reading finished
     finishCurrentBook() {

          if (this.listUnreadBook === null) {

               this.curBook = null;
               this.nextBook = null;

               console.log('list book empty!');
          }
          else {

               if (this.curBook !== null) {

                    this.curBook.haveRead(true, Date.now());
                    this.completeBook.push(this.curBook);
               }

               this.curBook = this.listUnreadBook[0];
               this.listUnreadBook.shift();

               if (this.listUnreadBook !== null) {

                    this.nextBook = this.listUnreadBook[0];
               }
          }
     }

     getCurBook() {                     //get book is currently reading

          return this.curBook;
     }

     getComplete() {                    //get list book is complete

          return this.completeBook;
     }

     getListUnread() {                  //get list book isn't read

         return this.listBook;
     }

     getNextBook() {                    //get the next book will read

          return this.nextBook;
     }
}

let book = new Book('abc', 'machine learning', 'smay');
let list = new BookList();

list.add(book);
list.add(new Book('xyz', 'literary', 'meo'));



////////////////////// updating... ///////////////////////////
